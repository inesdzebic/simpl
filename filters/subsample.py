## begin fhead
#
# funcname "subsample"
# name "Subsample"
# desc "Subsample image by factor you choose"
# arg "sub_number" spinbox int [0, inf] "Subsample factor"
#
## end fhead



import cv2
import numpy as np 
import math

def subsample (img, args):
    sub_number=args['sub_number']
    if len(img.shape)==3:
        img_blue = sub_funkcija(img[:, :, 0], sub_number)
        img_green = sub_funkcija(img[:, :, 1], sub_number)
        img_red = sub_funkcija(img[:, :, 2], sub_number)
        img_subsampled = np.dstack((img_blue, img_green, img_red))
        slika=img_subsampled

    else:
        slika=sub_funkcija(img, sub_number)



    return slika.astype(np.uint8)




def sub_funkcija(img, sub_number):        
    redak = img.shape[0]
    stupac = img.shape[1]
    novi_redak=(int(redak/sub_number))*sub_number
    novi_stupac=(int(stupac/sub_number))*sub_number
    img_crop=img 
    img_crop[0]=novi_redak
    img_crop[1]=novi_stupac
    for i in range (0, img_crop.shape[0]):
        for j in range (0, img_crop.shape[1]):
            if((i % sub_number == 0) and (j % sub_number == 0)):
                avg = int(math.floor(np.average(img_crop[i:i+sub_number, j:j+sub_number])))
                img_crop[i:i+sub_number, j:j+sub_number] = avg
            
    return img_crop.astype(np.uint8)


   

     
        