## begin fhead
#
# funcname "imghist"
# name "Histogram Equalisation"
# desc ""
# arg "he" checkbox "Histogram equalisation?"
# 
## end fhead

import cv2
import numpy as np 

def imghist(img,args):
    
    he=args['he']
    if (he==True):
        
        if (len(img.shape) == 2): 
            slika_hieq = hieq(img)
            return slika_hieq
           
        elif(len(img.shape) == 3): 
            slika_blue = hieq(img[:, :, 0])
            slika_green = hieq(img[:, :, 1])
            slika_red = hieq(img[:, :, 2])
            slika_hieq = np.dstack((slika_blue, slika_green, slika_red))
            return slika_hieq
        
    else:
        return img



def hieq(img):
    
    redak=img.shape[0]
    stupac=img.shape[1]
    zbroj_pixela=redak*stupac
    img2=img.copy()
  
    br_pojav= np.zeros( (256), dtype=np.float64)
    cum= np.zeros( (256), dtype=np.float64)
    pomocna=0

        #prolazi kroz sve pixele, kroz sve intenzitete i povecava vrijednost u polju br_pojav
        # za 1 svaki put kada se odredeni intenzitet pojavi
    for i in range (0,redak):
        for j in range (0, stupac): 
            br_pojav[img2[i,j]] += 1
                       
        #u polje cum[] sprema kumulativne vrijednosti       
    for z in range (0,256):
        cum[z]=pomocna+br_pojav[z]
        pomocna=cum[z]
      
        #np.nonzero() ce vratiti sve vrijednosti koje nisu 0,
        # np.min ce uzeti od toga minimalnu vrijednost
    cdf_min =  np.min(np.nonzero(cum))

        #formula za histogram eq    
    heq = np.round( ((cum - cdf_min)/(zbroj_pixela - cdf_min)) * 255 )
    
        # ponovno prolazi kroz sve piksele i mijenja ih prema heq
    for y in range(img2.shape[0]):
        for x in range(img2.shape[1]):
            img2[y,x] = heq[img[y,x]]

    return img2