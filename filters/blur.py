## begin fhead
#
# funcname "gaussian_blur"
# name "Gaussian Blur"
# desc "Blur image using the Gaussian filter"
# arg "r_h" spinbox int [0, inf] "Horizontal radius"
# arg "r_v" spinbox int [0, inf] "Vertical radius"
#
## end fhead

import cv2


def gaussian_blur(img, args):
    r_h = args['r_h']
    r_v = args['r_v']

    k_width = r_h * 2 + 1
    k_height = r_v * 2 + 1

    tmp = cv2.GaussianBlur(img, (k_width, k_height), 0)

    return tmp
