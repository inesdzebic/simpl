## begin fhead
#
# funcname "greyscale"
# name "Greyscale"
# desc "Convert to greyscale"
# arg "invert" checkbox "Invert"
#
## end fhead

import cv2
import numpy as np 

def greyscale (img, args):
    invert=args['invert']
    if len(img.shape)==3:
        temp=img.astype(np.float32)
        g=0.0722*temp[:,:,0]+0.7152*temp[:,:,1]+0.2126*temp[:,:,2]
        
        
    else:
        g=img

    if invert==True:
        g=255-g

    return g.astype(np.uint8)

    
          



    