# -*- coding: utf-8 -*-
import string
import imp
import inspect
from numpy import inf

class ArgUIWidget:
    # enums need no constructors as it would have nothing to do
    QSpinBox, QDoubleSpinBox, QComboBox, QCheckBox = range(4)

class ArgDataType:
    Int, Float = range(2)


class Argument:
    def __init__(self, _kname = "", _uiwidget = None, _type = None, _ll = None, _lu = None, _gname = "", _strvalues = None):
        self.key_name = _kname
        self.ui_widget = _uiwidget
        self.type = _type
        self.limit_lower = _ll
        self.limit_upper = _lu
        self.string_values = _strvalues

        if _gname == "":
            self.gui_name = _kname
        else:
            self.gui_name = _gname


class FilterMeta:
    def __init__(self, _name = "", _desc = "", _funcname = "", _args = None):
        self.name = _name
        self.desc = _desc
        self.funcname = _funcname
        self.args = _args

        # TODO[FR]: handle no-arg scenarios
        # -> store a functionProtoHasArgs bool in FilterMeta so that the filter
        #    could be called properly in filter_handler()
        #     -> True in case of fname(img, args)
        #     -> False in case of fname(img)
        # -> compare with argument count once the list is constructed in
        #    parse_fhead() and throw an error if there's a mismatch
        #    -> if filter author did fPHA = True and len(FM.args) == 0, fine.
        #       just send it an empty args list.
        #
        # initial: send everyone args and expect everyone to have fPHA = False



def parse_fhead(path):
    ffile = open(path, 'r')

    # how the parse_fhead() method works:
    # (1) find "## begin fhead"
    #  *** separate keywords with spaces, spaces elsewhere
    #      ("##begin" or "## begin", both handled) are a matter of
    #      taste ***
    #  *** if no "## begin fhead" is found, function head will
    #      NOT be entered at all, barring proper filter definition ***
    #  - if everything is okay, we should now be in the filter head
    # (2) every following line should start with a '#'. everything
    #     else is invalid syntax (parsing terminated and filter
    #     ignored)
    #  *** bare minimum in the definition is funcname - we need to
    #      know what we're going to call when the user presses the
    #      filter in the Filters menu, otherwise None again ***
    # (3) find "## end fhead"
    # (4) profit! (exiting fhead hoping that the filter author did everything
    #     else properly)

    fm_funcname = ""

    # description is optional, arguments too
    fm_desc = ""
    fm_args = []

    # initial default for name is an empty string ....
    fm_name = ""


    fhead_entered = False

    ffilename = path.split('/')
    ffilename = ffilename[len(ffilename) - 1]

    lineno = 0

    for line in ffile:
        # increment the line number counter
        lineno += 1

        ltmp = line.rstrip()

        # ltmps - line, temp, split
        ltmps = ltmp.split(' ')

        # remove unnecessary spaces after splitting
        for i in range(ltmps.count('')):
            ltmps.remove('')

        # seeking for the fhead beginning
        # both "##begin fhead" and "## begin fhead" are okay
        if not fhead_entered:
            fhead_entered = parse_fhead_checkkwfhead(ltmps, "begin")
        elif parse_fhead_checkkwfhead(ltmps, "end"):
            break
        else:
            # in head, so let's see what we've got here
            # empty lines are unacceptable (2)
            if len(ltmps) == 0:
                print ffilename + "[" + str(lineno) + "]" + ": " + "Empty lines are not allowed inside the filter header. Parsing aborted."
                return None
            else:
                # TODO[RS]: fuse this with checkkwffhead
                # ... hmm... does it even make sense?
                # handling keywords glued to the hash sign
                if ltmps[0] != '#' and ltmps[0][0] == '#':
                    tmp = ltmps[0].lstrip('#')
                    ltmps.remove(ltmps[0])
                    ltmps.insert(0, tmp)
                    ltmps.insert(0, '#')

                if ltmps[0] == '#':
                    if len(ltmps) >= 3:
                        if ltmps[1] == "funcname":
                            tmp = parse_fhead_cqrs(ltmps[2])
                            if tmp is not None:
                                fm_funcname = tmp
                                # TODO[RS]: make an eco-friendly and ...
                                # ... pythonic (sprintf-esque) (exceptions?)
                                # error printing mechanism

                                # TODO: check if non-limitable types are ...
                                # ... limited. ret none if someone does that
                                # ... or just ignore limits?
                            else:
                                print ffilename + "[" + str(lineno) + "]" + ": " + "Invalid argument provided for 'funcname'. Parsing aborted."
                                return None
                        elif ltmps[1] == "name":
                            # take everything after 'name'
                            # join with sep ' '
                            tmp = parse_fhead_cqrs(
                                parse_fhead_joinxtoend(ltmps, 2)
                            )
                            if tmp is not None:
                                fm_name = tmp
                            else:
                                print ffilename + "[" + str(lineno) + "]" + ": " + "Invalid argument provided for 'name'. Parsing aborted."
                                return None
                        elif ltmps[1] == "desc":
                            tmp = parse_fhead_cqrs(
                                parse_fhead_joinxtoend(ltmps, 2)
                            )
                            if tmp is not None:
                                fm_desc = tmp
                            else:
                                print ffilename + "[" + str(lineno) + "]" + ": " + "Invalid argument provided for 'desc'. Parsing aborted."
                                return None
                        elif ltmps[1] == "arg":
                            # going piecewise. argument's full name can have one
                            # or more words.
                            # first, let's check if the arg line has a minimum
                            # of 5 keywords
                            if len(ltmps) >= 4:
                                # shortest valid argument description line is
                                # four (4) elements long
                                # e.g. "# arg "example" checkbox"

                                # as the bare minimum is met, let's see if the
                                # words are right

                                # is [2] a string?
                                arg_kname = parse_fhead_cqrs(ltmps[2])
                                if arg_kname is None:
                                    print ffilename + "[" + str(lineno) + "]" + ": " + "Malformed argument key name. Parsing aborted."
                                    return None
                                # is [2] unique?
                                if len(fm_args) != 0:
                                    for arg in fm_args:
                                        if arg.key_name == arg_kname:
                                            print ffilename + "[" + str(lineno) + "]" + ": " + "Key name not unique. Parsing aborted."
                                            return None

                                if ltmps[3] == "spinbox":
                                    # len(ltmps) >= 4 makes sure [3] is
                                    # accessible, let's apply the same logic
                                    # and check for [4]
                                    if len(ltmps) >= 5:
                                        if ltmps[4] == "int":
                                            arg_type = ArgDataType.Int
                                            arg_uiw = ArgUIWidget.QSpinBox
                                        elif ltmps[4] == "float":
                                            arg_type = ArgDataType.Float
                                            arg_uiw = ArgUIWidget.QDoubleSpinBox
                                        else:
                                            # there are no doubles in Python, so
                                            # it isn't valid syntax here either
                                            print ffilename + "[" + str(lineno) + "]" + ": " + "Invalid data type specified for argument '" + arg_kname + "'. Parsing aborted."
                                            return None

                                        if len(ltmps) >= 6:
                                            # check if [5] starts with a '['
                                            if ltmps[5][0] == '[':
                                                # it's an interval!
                                                # is the matching bracket at the
                                                # same index as well?
                                                if ltmps[5][len(ltmps[5]) - 1] == ']':
                                                    # strip '[' and ']' and
                                                    # split by commas, remove
                                                    # spaces
                                                    lim = parse_fhead_genlistfrombracketedstring(ltmps[5])
                                                    gui_name_idx = 6
                                                    # we'll do parsing later.
                                                else:
                                                    tmpj = parse_fhead_smbj(ltmps, 5)

                                                    if tmpj is None:
                                                        print ffilename + "[" + str(lineno) + "]" + ": " + "Parsing range for argument " + arg_kname + " failed. Parsing aborted."
                                                        return None

                                                    lim = parse_fhead_genlistfrombracketedstring(tmpj[0])
                                                    gui_name_idx = tmpj[1]

                                                if len(lim) == 2:
                                                    if lim[0] == "-inf":
                                                        arg_ll = -inf
                                                    elif lim[0] == "inf":
                                                        print ffilename + "[" + str(lineno) + "]" + ": " + "Lower limit can not be infinite. Parsing aborted."
                                                        return None
                                                    else:
                                                        arg_ll = parse_fhead_tryparsenum(lim[0], arg_type)

                                                        if arg_ll is None:
                                                            print ffilename + "[" + str(lineno) + "]" + ": " + "Parsing lower limit for argument '" + arg_kname + "' failed. Parsing aborted."
                                                            return None

                                                    if lim[1] == "inf":
                                                        arg_lu = inf
                                                    elif lim[1] == "-inf":
                                                        print ffilename + "[" + str(lineno) + "]" + ": " + "Upper limit can not be negatively infinite. Parsing aborted."
                                                        return None
                                                    else:
                                                        arg_lu = parse_fhead_tryparsenum(lim[1], arg_type)

                                                        if arg_lu is None:
                                                            print ffilename + "[" + str(lineno) + "]" + ": " + "Parsing upper limit for argument '" + arg_kname + "' failed. Parsing aborted."
                                                            return None

                                                    if arg_ll >= arg_lu:
                                                        # in case someone tries to bork this part
                                                        print ffilename + "[" + str(lineno) + "]" + ": " + "Lower limit greater than or equal to the upper limit. Parsing aborted."
                                                        return None

                                                    # clean up infinite limits
                                                    # so that they can be stored
                                                    # in an Argument object for
                                                    # later evaluation in
                                                    # QSFilterDialog
                                                    if lim[0] == "-inf":
                                                        arg_ll = None
                                                    if lim[1] == "inf":
                                                        arg_lu = None

                                                    if len(ltmps) >= gui_name_idx + 1:
                                                        arg_gname = parse_fhead_cqrs(parse_fhead_joinxtoend(ltmps, gui_name_idx))

                                                        if arg_gname is None:
                                                            # something is here,
                                                            # but it doesn't
                                                            # have good
                                                            # intentions
                                                            print ffilename + "[" + str(lineno) + "]" + ": " + "Malformed argument GUI name. Parsing aborted."
                                                            return None

                                                        # construct a named,
                                                        # limited argument
                                                        tmp = Argument(arg_kname, arg_uiw, arg_type, arg_ll, arg_lu, arg_gname)
                                                        fm_args.append(tmp)

                                                    else:
                                                        # nothing followed the
                                                        # limits.
                                                        # construct GUI unnamed,
                                                        # limited Argument here
                                                        # then.
                                                        # TODO: reduce Argument calls by defaulting values?
                                                        # ... yeah, sure, but it seems more readable this way
                                                        # ... perhaps when the fheadparser gets feature frozen...
                                                        tmp = Argument(arg_kname, arg_uiw, arg_type, arg_ll, arg_lu)
                                                        fm_args.append(tmp)

                                                else:
                                                    print ffilename + "[" + str(lineno) + "]" + ": " + "Malformed value range for argument '" + arg_kname + "'. Parsing aborted."
                                                    return None

                                            else:
                                                # then it must be a GUI name.
                                                arg_gname = parse_fhead_cqrs(parse_fhead_joinxtoend(ltmps, 5))

                                                if arg_gname is None:
                                                    # user probably failed and
                                                    # entered some invalid
                                                    # syntax here
                                                    print ffilename + "[" + str(lineno) + "]" + ": " + "Malformed argument GUI name. Parsing aborted."
                                                    return None

                                                # construct a named, unlimited
                                                # Argument
                                                tmp = Argument(arg_kname, arg_uiw, arg_type, None, None, arg_gname)
                                                fm_args.append(tmp)

                                        else:
                                            # widget is unnamed and unlimited
                                            tmp = Argument(arg_kname, arg_uiw, arg_type)
                                            fm_args.append(tmp)

                                    else:
                                        print ffilename + "[" + str(lineno) + "]" + ": " + "'spinbox' widget defined with an inadequate number of arguments. Parsing aborted."
                                        return None

                                elif ltmps[3] == "dropdown":
                                    if len(ltmps) >= 5:
                                        arg_uiw = ArgUIWidget.QComboBox

                                        if ltmps[4][0] == '[':
                                            # okay, that's a beginning.
                                            # let's see if [4] contains a matching bracket too.
                                            if ltmps[4][len(ltmps[4]) - 1] == ']':
                                                tmp = parse_fhead_genlistfrombracketedstring(ltmps[4])
                                                arg_strvals = []

                                                for s in tmp:
                                                    t = parse_fhead_cqrs(s)
                                                    if t is None:
                                                        print ffilename + "[" + str(lineno) + "]" + ": " + "Malformed option string. Parsing aborted."
                                                        return None
                                                    arg_strvals.append(t)

                                                gui_name_idx = 5
                                            else:
                                                tmpj = parse_fhead_smbj(ltmps, 4)

                                                if tmpj is None:
                                                    print ffilename + "[" + str(lineno) + "]" + ": " + "Parsing option strings for argument '" + arg_kname + "' failed. Parsing aborted."
                                                    return None

                                                tmp = parse_fhead_genlistfrombracketedstring(tmpj[0])
                                                gui_name_idx = tmpj[1]

                                                arg_strvals = []

                                                for s in tmp:
                                                    t = parse_fhead_cqrs(s)
                                                    if t is None:
                                                        print ffilename + "[" + str(lineno) + "]" + ": " + "Malformed option string. Parsing aborted."
                                                        return None
                                                    arg_strvals.append(t)


                                            if len(ltmps) >= gui_name_idx + 1:
                                                arg_gname = parse_fhead_cqrs(parse_fhead_joinxtoend(ltmps, gui_name_idx))

                                                if arg_gname is None:
                                                    # gibberish found...
                                                    print ffilename + "[" + str(lineno) + "]" + ": " + "Malformed argument GUI name. Parsing aborted."
                                                    return None

                                                # otherwise, construct a named
                                                # Argument
                                                tmp = Argument(arg_kname, arg_uiw, None, None, None, arg_gname, arg_strvals)
                                                fm_args.append(tmp)

                                            else:
                                                # nothing followed the option
                                                # string list
                                                # construct GUI unnamed Argument
                                                # here then.
                                                tmp = Argument(arg_kname, arg_uiw, None, None, None, "", arg_strvals)
                                                fm_args.append(tmp)

                                        else:
                                            print ffilename + "[" + str(lineno) + "]" + ": " + "No options provided for the 'dropdown' widget. Parsing aborted."
                                    else:
                                        print ffilename + "[" + str(lineno) + "]" + ": " + "'dropdown' widget defined with an inadequate number of arguments. Parsing aborted."
                                        return None

                                elif ltmps[3] == "checkbox":
                                    arg_uiw = ArgUIWidget.QCheckBox
                                    if len(ltmps) >= 5:
                                        # oh, a GUI name?
                                        arg_gname = parse_fhead_cqrs(parse_fhead_joinxtoend(ltmps, 4))

                                        if arg_gname is None:
                                            # it's not a GUI name, it's
                                            # gibberish
                                            print ffilename + "[" + str(lineno) + "]" + ": " + "Malformed argument GUI name. Parsing aborted."
                                            return None

                                        # otherwise, construct a named
                                        # Argument
                                        tmp = Argument(arg_kname, arg_uiw, None, None, None, arg_gname)
                                        fm_args.append(tmp)

                                    else:
                                        tmp = Argument(arg_kname, arg_uiw)
                                        fm_args.append(tmp)
                                else:
                                    print ffilename + "[" + str(lineno) + "]" + ": " + "Unknown UI widget type specified. Parsing aborted."
                                    return None

                            else:
                                # if bare minimum not met
                                print ffilename + "[" + str(lineno) + "]" + ": " + "Argument defined with an inadequate number of parameters. Parsing aborted."
                                return None

                        else:
                            print ffilename + "[" + str(lineno) + "]" + ": " + "'" + ltmps[1] + "' is not a valid keyword. Parsing aborted."
                            return None

                    elif len(ltmps) == 1:
                        # just an empty line. that is acceptable.
                        # but we can't do anything useful with it, so continue
                        continue
                    else:
                        print ffilename + "[" + str(lineno) + "]" + ": " + "Malformed command: '" + ltmp + "'. Parsing aborted."
                        return None
                else:
                    # ...and so is a line which starts with gibberish
                    print ffilename + "[" + str(lineno) + "]" + ": " + "'" + ltmp + "'" + " is not a valid way to start a line inside a SIMPL filter header. Parsing aborted."
                    return None

    # .... but if name remains empty until now, we assume the filter's name is
    # its Python file name
    if fm_name == '':
        fm_name = ffilename

    if fm_funcname == "":
        print ffilename + ": " + "Filter function name can not be empty. Parsing aborted."
        return None
    else:
        # let's check if there really is a function called fm_funcname
        fn_noext = ffilename.rsplit('.', 1)[0]
        mod_name = string.join(["filters-", fn_noext], '')

        try:
            mod = imp.load_source(mod_name, path)
        except Exception as e:
            print ffilename + ": " + "Filter file contains invalid Python syntax. Parsing aborted. The error reported was: \n\t" + e.__class__.__name__ + ": " + str(e)
            return None
        else:
            funcs = inspect.getmembers(mod, inspect.isfunction)

            func_found = False

            for ftuple in funcs:
                if ftuple[0] == fm_funcname:
                    func_found = True

                    fcode = ftuple[1].func_code
                    arg_count = fcode.co_argcount

                    # filter function should have two arguments
                    if arg_count != 2:
                        print ffilename + "[" + str(lineno) + "]" + ": " + "Function " + fm_funcname + " should have 2 arguments. Found " + str(arg_count) + ". Parsing aborted."
                        return None
                    else:
                        # we found the function, its signature is valid.
                        # we're done here, so let's exit the loop.
                        break

            if not func_found:
                print ffilename + ": " + "No function called '" + fm_funcname + "'. Parsing aborted."
                return None

    if len(fm_args) == 0:
        fm_args = None

    data = FilterMeta(fm_name, fm_desc, fm_funcname, fm_args)
    return data


def parse_fhead_checkkwfhead(ln, kw):
    """
    Checks if the given line is a proper beginning or ending of
    the filter's header, depending on the keyword provided. Returns
    True if syntax is correct and a beginning or ending is detected,
    otherwise False.

    :param ln: list of strings with the trailing newline stripped
    :type ln: list of str
    :param kw: 'begin' or 'end'
    :type kw: str

    :rtype: bool
    """

    if len(ln) == 0:
        return False

    kw_prefixed = string.join(["##", kw], '')

    # "##KW" -> "fhead" (length: 2)
    # e.g. "##begin" -> "fhead"
    if len(ln) == 2:
        if ln[0] == kw_prefixed:
            if ln[1] == "fhead":
                return True
            else:
                return False
        else:
            return False
    # "##" -> "KW" -> "fhead" (length: 3)
    elif len(ln) == 3:
        if ln[0] == "##":
            if ln[1] == kw:
                if ln[2] == "fhead":
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False
    else:
        return False


def parse_fhead_cqrs(s):
    """
    Checks if the input string is properly surrounded with quotes.
    Returns None if the quotes are not properly matched, otherwise
    a string without quotes or apostrophes.

    :param s: string to be validated and stripped
    :type s: str

    :rtype: str/None
    """

    # this function always receives strings longer than zero
    # characters, so no validation of such sort will be performed
    # here
    if s[0] == '\'' and s[len(s) - 1] == '\'':
        tmp = s.lstrip('\'')
        tmp = tmp.rstrip('\'')
    elif s[0] == '\"' and s[len(s) - 1] == '\"':
        tmp = s.lstrip('\"')
        tmp = tmp.rstrip('\"')
    else:
        return None

    return tmp


def parse_fhead_genlistfrombracketedstring(s):
    """
    Takes a string surrounded with (square) brackets containing a
    comma-separated list of items with arbitrary whitespace. Generates a list
    object with all the previously mentioned elements removed.

    :param s: string to be stripped and split
    :type s: str

    :rtype: list of str
    """

    tmp = s.replace('[', '', 1)
    tmp = tmp.replace(']', '', 1)
    tmp = tmp.split(',')
    ret = []
    for s in tmp:
        ret.append(s.strip())

    return ret


def parse_fhead_smbj(lst, start):
    """
    Takes a list of strings and an integer index. The list of strings should
    start by an opening (square) bracket and end with a matching closing one.
    If the matching bracket is found, the function returns a joined string
    surrounded with brackets and the index of the element that follows the one
    where the matching bracket is found. If the search fails, returns None.

    :param lst: list of strings
    :type lst: list of str
    :param start: integer index from which the join should start
    :type start: int

    :rtype: list/None
    """

    # start contains the string starting with '['. we assume that lst[start]
    # doesn't end with a ']' and that we have to seek for it in the elements
    # that follow.
    i = start + 1
    tmp = None
    for s in lst[i : len(lst)]:
        if s[len(s) - 1] == ']':
            # join() separator is ' ' to make sure strings which have spaces
            # don't lose them in the process (dropdown primarily benefits from
            # this change)
            # parse_fhead_genlistfrombracketedstring() will strip the excess
            # spaces anyway
            tmp = string.join(lst[start : i + 1], ' ')
            break
        else:
            i += 1
    if tmp is not None:
        return [tmp, i + 1]
    else:
        return None


def parse_fhead_joinxtoend(lst, x):
    """
    Joins strings in a list of strings from the xth element to the last one.

    :type lst: list
    :type x: int

    :rtype: str
    """

    tmp_l = lst[x: len(lst)]
    tmp = string.join(tmp_l, ' ')
    return tmp


def parse_fhead_tryparsenum(n, t):
    """
    Will try to parse the given number as int or float, depending on the
    ArgDataType. If a float is provided and ArgDataType is Int, it will be
    truncated. Returns None if parsing fails.

    :param n: number to be parsed
    :type n: int/float
    :param t: argument type
    :type t: ArgDataType

    :rtype: int/float/None
    """

    try:
        if t == ArgDataType.Int:
            tmp = int(n)
        elif t == ArgDataType.Float:
            tmp = float(n)
        # no else block, let's hope no rogue data comes in here
        return tmp
    except:
        return None
